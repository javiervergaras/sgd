import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JerarquiaContextoComponent } from './jerarquia-contexto.component';

describe('JerarquiaContextoComponent', () => {
  let component: JerarquiaContextoComponent;
  let fixture: ComponentFixture<JerarquiaContextoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JerarquiaContextoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JerarquiaContextoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
