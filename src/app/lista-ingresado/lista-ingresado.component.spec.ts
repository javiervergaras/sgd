import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaIngresadoComponent } from './lista-ingresado.component';

describe('ListaIngresadoComponent', () => {
  let component: ListaIngresadoComponent;
  let fixture: ComponentFixture<ListaIngresadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaIngresadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaIngresadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
