import { Component ,OnInit } from '@angular/core';
import { TreeViewComponent, NodeSelectEventArgs } from '@syncfusion/ej2-angular-navigations';
import { ApiService } from '../api.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-jerarquia-contexto',
  templateUrl: './jerarquia-contexto.component.html',
  styleUrls: ['./jerarquia-contexto.component.css']
})
export class JerarquiaContextoComponent implements OnInit {
  jerarquia:any;
  treeFields:Object;
  empresas:any;
  closeResult: string;
  name:string;
  nodo:number;
  modal:NgbModalRef;
  editSelected:string;
  
                                                                                          
  constructor(
    public apiService: ApiService,
    private modalService: NgbModal,
    private toastr:ToastrService,
    ) {
      this.name = "";
    this.cargaempresas();
    
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  open(content) {
    this.modal = this.modalService.open(content);
    
  }
  
  async cargaempresas(){
    this.empresas = await this.apiService.getEmpresas().toPromise();
    console.log(this.empresas);
    this.cargaJerarquia(this.empresas[0].IDEmpresa);
  }
  
  ngOnInit() {
  
  }
  
  async cargaJerarquia(empresa){
    this.jerarquia = await this.apiService.getJerarquia(empresa,2).toPromise();
    let _jerarquiaAux:any;
    for(let x of this.jerarquia){
      _jerarquiaAux = await this.jerarquia.filter(y=> y.pid === x.id);
      if(_jerarquiaAux.length > 0){
        x.hasChild = true
      }
      _jerarquiaAux = await this.jerarquia.filter(y=> y.id === x.pid);
      if(_jerarquiaAux.length == 0){
        delete x['pid'];
      }
  
    }
    console.log(this.jerarquia); 
  this.treeFields = {
    dataSource: this.jerarquia, 
    id: 'id', 
    parentID: 'pid', 
    text: 'name', 
    hasChildren: 'hasChild'
  }; 
  }
  
  nodeSelected(e: any) {
    this.nodo = e.data[0].id;
    console.log(e.data[0]);
    this.editSelected = e.data[0].text;
    console.log(this.editSelected);
  };
  
  async guardar(empresa){
    
    if (this.jerarquia.length > 0){
      if( this.name.length == 0 || (!this.nodo) ){
        alert('Existe un problema y no podemos continuar, asegurese de seleccionar un nodo o ingresar un nombre para poder continuar');
            }else{
        
              try{
                console.log(this.nodo);
                await this.apiService.setNodo(this.nodo,this.name,2,empresa.value).toPromise();
                await this.cargaJerarquia(empresa.value);
                this.name = "";
                this.modal.dismiss();
                this.toastr.success('El Nodo fue agregado con exito','Acción realizada');
              }catch(error){
                console.log(error);
              }
            }
        
    }else{
      await this.apiService.setNodo(0,this.name,2,empresa.value).toPromise();
      await this.cargaJerarquia(empresa.value);
      this.name = "";
      this.modal.dismiss();
      this.toastr.success('El Nodo fue agregado con exito','Acción realizada');
    } 
  }

  edit(edit){
    if(this.nodo){
      this.modal = this.modalService.open(edit);
    }
  }

  async editselectedrow(empresa){
    await this.apiService.editNodo(this.nodo,this.editSelected,2).toPromise();
    this.modal.dismiss();
    await this.cargaJerarquia(empresa.value);
    this.editSelected = "";
    this.toastr.success('El Nodo fue editado con exito','Acción realizada');
  }

  async change(empresa){
    await this.cargaJerarquia(empresa);
    this.editSelected = "";
  }

  eliminar(eliminar){
    console.log("Llego");
    if(this.nodo){
      console.log(this.nodo);
      let cantidad = this.jerarquia.filter(x=> x.pid == this.nodo);
      console.log(cantidad);
      if(cantidad.length > 0) {
        this.toastr.error('No se puede elimar el nodo seleccionado, elimine los sub nodos y luego intente nuevamente','Error');
      }else{
        this.modal = this.modalService.open(eliminar);
      }
    }
  }

  async eliminarNodo(empresa){
    this.apiService.del_nodo(this.nodo,2).toPromise();
    this.toastr.success('El nodo fue eliminado satisfactoriamente','Acción realizada');
    await this.cargaJerarquia(empresa.value);
    this.modal.dismiss();
  }

  }
