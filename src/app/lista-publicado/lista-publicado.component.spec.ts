import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPublicadoComponent } from './lista-publicado.component';

describe('ListaPublicadoComponent', () => {
  let component: ListaPublicadoComponent;
  let fixture: ComponentFixture<ListaPublicadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPublicadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPublicadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
