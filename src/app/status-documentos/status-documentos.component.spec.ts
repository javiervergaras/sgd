import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusDocumentosComponent } from './status-documentos.component';

describe('StatusDocumentosComponent', () => {
  let component: StatusDocumentosComponent;
  let fixture: ComponentFixture<StatusDocumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusDocumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
