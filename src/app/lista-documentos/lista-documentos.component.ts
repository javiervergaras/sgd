import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-lista-documentos',
  templateUrl: './lista-documentos.component.html',
  styleUrls: ['./lista-documentos.component.css']
})
export class ListaDocumentosComponent implements OnInit {
  studentsData: any = {};
  docs:any;
  empresas:any;
  idDocument:any;
  modal:NgbModalRef;
  empresa:any;

  async cargaempresas(){
    this.empresas = await this.apiService.getEmpresas().toPromise();
    console.log(this.empresas);
    //this.cargaJerarquia(this.empresas[0].IDEmpresa);
  }

  async getAllStudents() {
    //Get saved list of students
    this.studentsData = await this.apiService.getBorradores(this.empresa).toPromise();
  }

  change(){
    this.getAllStudents();
  }

  async pasarRevision(id:string,idempresa:string){
    let _mensaje:any = await this.apiService.setRevision(id,"2",idempresa).toPromise();
    console.log(_mensaje);
    if(_mensaje.estado == 1){
     this.toastr.success(_mensaje.mensaje,'Acción realizada');
    }else{
     this.toastr.error(_mensaje.mensaje,'Acción no realizada');
    }
    
 }

  refresh(){
   this.getAllStudents();
  }

  constructor(private toastr: ToastrService,public apiService: ApiService, private modalService: NgbModal,public spinner:NgxSpinnerService,) {
    this.empresa = 0;
    this.cargaempresas();
    this.studentsData = [];
    this.docs = [];
    
   }

   showSpinner() {
    this.spinner.show();
  }

  hideSpinner(){
    this.spinner.hide();
  }

   async verDoc(empresa,codigo,nombre){
     this.showSpinner();
    let _id = (empresa + '-' + codigo)
    console.log(_id);
    let _file:any = await this.apiService.download_file(_id).toPromise();
    console.log(_file.file);
     // 1, "string", false
    const blob = this.b64toBlob(_file.file, 'application/octet-stream');
    console.log(blob);
    let url = window.URL.createObjectURL(blob);
    const file = new File([blob], nombre, { type: 'application/octet-stream' });
    await saveAs(file);
    this.hideSpinner();
   }

   b64toBlob (b64Data, contentType='', sliceSize=512){
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
  
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
  
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
  
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
      
    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }


  ngOnInit() {
    this.getAllStudents();
    console.log("Paso por metodo");
  }
  elements: any = [
    {id: 1, first: 'Mark', last: 'Otto', handle: '@mdo'},
    {id: 2, first: 'Jacob', last: 'Thornton', handle: '@fat'},
    {id: 3, first: 'Larry', last: 'the Bird', handle: '@twitter'},
  ];

  headElements = ['ID', 'First', 'Last', 'Handle'];

  abrirPopUp(id,modal){
    this.idDocument = id;
    this.modal = this.modalService.open(modal);
  }

  async actualizar(status){
    console.log(this.idDocument);
    console.log(status);
    this.apiService.setStatus(this.idDocument,status).toPromise();
    this.modal.dismiss();
    await this.refresh();
  }

}