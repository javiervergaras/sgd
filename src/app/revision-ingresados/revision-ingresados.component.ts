import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-revision-ingresados',
  templateUrl: './revision-ingresados.component.html',
  styleUrls: ['./revision-ingresados.component.css']
})
export class RevisionIngresadosComponent implements OnInit {
  studentsData: any;
  docs:any;
  empresas:any;
  empresa:any;
  
  constructor(private toastr: ToastrService,public apiService: ApiService,public spinner:NgxSpinnerService) {
    this.cargaempresas();
    this.studentsData = [];
    this.docs = [];
    this.empresa = 0;
   }

   async cargaempresas(){
    this.empresas = await this.apiService.getEmpresas().toPromise();
    console.log(this.empresas);
    //this.cargaJerarquia(this.empresas[0].IDEmpresa);
  }

   ngOnInit() {
    this.getAllStudents();
    console.log("Paso por metodo");
  }

  async getAllStudents() {
    //Get saved list of students
   this.studentsData = await this.apiService.getRevisiones(this.empresa,'2').toPromise();
   console.log(this.studentsData);
  }

  async change(){
    this.studentsData = await this.apiService.getRevisiones(this.empresa,'2').toPromise();
    console.log(this.studentsData);
  }

  set_status(id:string){

  // this.apiService.setStatus(id,'mundo').subscribe((res)=>{
    // console.log("Created a customer");  
   //});
  }

  refresh(){
    this.getAllStudents();
   }

  
  elements: any = [
    {id: 1, first: 'Mark', last: 'Otto', handle: '@mdo'},
    {id: 2, first: 'Jacob', last: 'Thornton', handle: '@fat'},
    {id: 3, first: 'Larry', last: 'the Bird', handle: '@twitter'},
  ];

  headElements = ['ID', 'First', 'Last', 'Handle'];


  showSpinner() {
    this.spinner.show();
  }

  hideSpinner(){
    this.spinner.hide();
  }

  async verDoc(empresa,codigo,nombre){
    this.showSpinner();
   let _id = (empresa + '-' + codigo)
   console.log(_id);
   let _file:any = await this.apiService.download_file(_id).toPromise();
   console.log(_file.file);
    // 1, "string", false
   const blob = this.b64toBlob(_file.file, 'application/octet-stream');
   console.log(blob);
   let url = window.URL.createObjectURL(blob);
   const file = new File([blob], nombre, { type: 'application/octet-stream' });
   await saveAs(file);
   this.hideSpinner();
  }

  b64toBlob (b64Data, contentType='', sliceSize=512){
   const byteCharacters = atob(b64Data);
   const byteArrays = [];
 
   for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
     const slice = byteCharacters.slice(offset, offset + sliceSize);
 
     const byteNumbers = new Array(slice.length);
     for (let i = 0; i < slice.length; i++) {
       byteNumbers[i] = slice.charCodeAt(i);
     }
 
     const byteArray = new Uint8Array(byteNumbers);
     byteArrays.push(byteArray);
   }
     
   const blob = new Blob(byteArrays, {type: contentType});
   return blob;
 }

}


