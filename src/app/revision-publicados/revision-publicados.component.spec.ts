import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionPublicadosComponent } from './revision-publicados.component';

describe('RevisionPublicadosComponent', () => {
  let component: RevisionPublicadosComponent;
  let fixture: ComponentFixture<RevisionPublicadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisionPublicadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionPublicadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
