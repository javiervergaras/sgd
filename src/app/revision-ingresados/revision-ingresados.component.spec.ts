import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionIngresadosComponent } from './revision-ingresados.component';

describe('RevisionIngresadosComponent', () => {
  let component: RevisionIngresadosComponent;
  let fixture: ComponentFixture<RevisionIngresadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisionIngresadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionIngresadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
