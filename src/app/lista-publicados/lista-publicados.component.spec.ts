import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPublicadosComponent } from './lista-publicados.component';

describe('ListaPublicadosComponent', () => {
  let component: ListaPublicadosComponent;
  let fixture: ComponentFixture<ListaPublicadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPublicadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPublicadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
