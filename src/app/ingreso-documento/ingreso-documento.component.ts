import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingreso-documento',
  templateUrl: './ingreso-documento.component.html',
  styleUrls: ['./ingreso-documento.component.css']
})
export class IngresoDocumentoComponent implements OnInit {
  empresas:any;
  image:any;
  documento:any = {};
  IDEmpresa:any;
  CodigoDocumento:any;
  NumeroVersion:any;
  Nombre:any;
  Descripcion:any;
  TipoFormato:any;
  Contexto:any;
  PalabrasClaves:any;
  PalabraComienza:any;
  PalabraFinaliza:any;
  Estado:any;
  FechaLimiteDefinitivo:any;
  UsuarioCreadorDocumento:any;
  Confidencial:any;
  UsuarioCreador:any;
  FechaHoraCreacion:any;
  UsuarioUltimaActualizacion:any;
  Observaciones:any;
  EstatusFlujo:any;
  SelectedFile:File;
  jerarquia:any;
  treeFields:Object;
  jerarquiaContexto:Object;
  jerarquiaOrganizacional:Object;
  jerarquiaGeografica:Object;

  constructor( 
    public apiService: ApiService,
    public spinner:NgxSpinnerService,
    public toastr:ToastrService,
    public router:Router,
    ) { 
      this.cargaempresas();
      this.TipoFormato = 0;
      this.Estado = 0;
      this.IDEmpresa = 0;
      this.cargaJerarquiaCon(this.IDEmpresa);
      this.cargaJerarquiaOrg(this.IDEmpresa);
      this.cargaJerarquiaGeo(this.IDEmpresa);
    }

  ngOnInit() {
  }

  async changeListener($event)  {
    //this.showSpinner();
    //await this.readThis($event.target);
    this.SelectedFile = $event.target.files[0];
  }
  
  readThis(ifile): void {
    var file:File = ifile;
    var myReader:FileReader = new FileReader();
    var name = file.name;
    myReader.onloadend = (e) => {
      this.showSpinner();
      this.image = myReader.result;
      this.cargaDocument(this.image);
    }
    myReader.readAsDataURL(file);
  }

  async guardaDoc(f,name){
    let _mensaje:any;
    _mensaje = this.apiService.upload_f(f,name).toPromise();
    console.log(_mensaje);
    this.hideSpinner();
  }

  showSpinner() {
    this.spinner.show();
  }

  hideSpinner(){
    this.spinner.hide();
  }

  async cargaJerarquiaCon(empresa){
    this.jerarquia = await this.apiService.getJerarquia(empresa,2).toPromise();
    let _jerarquiaAux:any;
    for(let x of this.jerarquia){
      _jerarquiaAux = await this.jerarquia.filter(y=> y.pid === x.id);
      if(_jerarquiaAux.length > 0){
        x.hasChild = true
      }
      _jerarquiaAux = await this.jerarquia.filter(y=> y.id === x.pid);
      if(_jerarquiaAux.length == 0){
        delete x['pid'];
      }
  
    }
    console.log(this.jerarquia); 
  this.jerarquiaContexto = {
    dataSource: this.jerarquia, 
    id: 'id', 
    parentID: 'pid', 
    text: 'name', 
    hasChildren: 'hasChild'
  }; 
  }

  async cargaJerarquiaOrg(empresa){
    this.jerarquia = await this.apiService.getJerarquia(empresa,3).toPromise();
    let _jerarquiaAux:any;
    for(let x of this.jerarquia){
      _jerarquiaAux = await this.jerarquia.filter(y=> y.pid === x.id);
      if(_jerarquiaAux.length > 0){
        x.hasChild = true
      }
      _jerarquiaAux = await this.jerarquia.filter(y=> y.id === x.pid);
      if(_jerarquiaAux.length == 0){
        delete x['pid'];
      }
  
    }
    console.log(this.jerarquia); 
  this.jerarquiaOrganizacional = {
    dataSource: this.jerarquia, 
    id: 'id', 
    parentID: 'pid', 
    text: 'name', 
    hasChildren: 'hasChild'
  }; 
  }

  async cargaJerarquiaGeo(empresa){
    this.jerarquia = await this.apiService.getJerarquia(empresa,1).toPromise();
    let _jerarquiaAux:any;
    for(let x of this.jerarquia){
      _jerarquiaAux = await this.jerarquia.filter(y=> y.pid === x.id);
      if(_jerarquiaAux.length > 0){
        x.hasChild = true
      }
      _jerarquiaAux = await this.jerarquia.filter(y=> y.id === x.pid);
      if(_jerarquiaAux.length == 0){
        delete x['pid'];
      }
  
    }
    console.log(this.jerarquia); 
  this.jerarquiaGeografica = {
    dataSource: this.jerarquia, 
    id: 'id', 
    parentID: 'pid', 
    text: 'name', 
    hasChildren: 'hasChild'
  }; 
  }

  async cargaempresas(){
    this.empresas = await this.apiService.getEmpresas().toPromise();
  }

  valida(valor){
    if(valor){
      return valor;
    }else{
      return 'NA';
    }
  }

cargaDocument(file){

  console.log(this.valida(this.Nombre));
  
  this.documento.IDEmpresa = this.IDEmpresa ;
  this.documento.CodigoDocumento =  this.CodigoDocumento;
  this.documento.NumeroVersion = this.NumeroVersion;
  this.documento.Nombre = this.valida(this.Nombre);
  this.documento.Descripcion = this.valida(this.Descripcion);
  this.documento.TipoFormato = this.valida(this.TipoFormato);
  this.documento.PalabrasClaves = this.valida(this.PalabrasClaves);
  this.documento.PalabraComienza = this.valida(this.PalabraComienza);
  this.documento.PalabraFinaliza = this.valida(this.PalabraFinaliza);
  this.documento.Estado = this.valida(this.Estado);
  this.documento.FechaLimiteDefinitivo = (this.FechaLimiteDefinitivo.year + '-' +this.FechaLimiteDefinitivo.month +'-' + this.FechaLimiteDefinitivo.day );
  this.documento.UsuarioCreadorDocumento = 1;
  this.documento.Confidencial = this.valida(this.Confidencial);
  this.documento.UsuarioCreador = 'Jguerra';
  this.documento.FechaHoraCreacion = new Date();
  this.documento.UsuarioUltimaActualizacion = 'Jguerra';
  this.documento.Observaciones = this.valida(this.Observaciones);
  this.documento.EstatusFlujo = 1;
  this.documento.file = file;
  this.documento.NombreDocumento = this.SelectedFile.name;
  console.log(this.documento);
  let _mensaje = this.apiService.save_document(this.documento).toPromise();
  console.log(_mensaje);
  this.hideSpinner();
  this.toastr.success('Documento Agregado con exito','Correcto');
  this.router.navigateByUrl('/status');
}

  async guardarDoc(){
  if(this.SelectedFile && this.CodigoDocumento && this.NumeroVersion && this.TipoFormato && this.Estado && this.FechaLimiteDefinitivo && this.IDEmpresa){
  this.showSpinner();
  let resultado:any = await this.validadoc(this.IDEmpresa,this.CodigoDocumento);
  console.log(resultado);
    if(resultado[0].cantidad == "0"){
      await this.readThis(this.SelectedFile);
    }else{
      this.toastr.error('El codigo de documento ya existe para esta empresa','Error');
      this.hideSpinner();
    }
  }else{
    this.toastr.error('Asegurese de llenar los campos necesarios para continuar','Error'); 
    this.hideSpinner();
  }
    
  }

  async validadoc(empresa,codigo){
    return this.apiService.valida_doc(empresa,codigo).toPromise();
  }

  cambiaEmpresa(){
    console.log(this.IDEmpresa);
    this.cargaJerarquiaCon(this.IDEmpresa);
    this.cargaJerarquiaOrg(this.IDEmpresa);
    this.cargaJerarquiaGeo(this.IDEmpresa);
  }
}
