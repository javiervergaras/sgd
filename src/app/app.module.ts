import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';


import { ListViewModule } from '@syncfusion/ej2-angular-lists';

import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
//complementos
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from "ngx-spinner";

import { TreeViewModule  } from '@syncfusion/ej2-angular-navigations';
import { HttpClientModule } from '@angular/common/http';
import { JerarquiaGeograficaComponent } from './jerarquia-geografica/jerarquia-geografica.component';
import { JerarquiaContextoComponent } from './jerarquia-contexto/jerarquia-contexto.component';
import { JerarquiaOrganizacionalComponent } from './jerarquia-organizacional/jerarquia-organizacional.component';
import { AppRoutingModule } from './app-routing.module';
import { DocumentosComponent } from './documentos/documentos.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { FormularioComponent } from './formulario/formulario.component';
import { ListaAprobadoComponent } from './lista-aprobado/lista-aprobado.component';
import { ListaDocumentosComponent } from './lista-documentos/lista-documentos.component';
import { ListaPublicadosComponent } from './lista-publicados/lista-publicados.component';
import { ListaPublicadoComponent } from './lista-publicado/lista-publicado.component';
import { ListaRevisadoComponent } from './lista-revisado/lista-revisado.component';
import { ListaIngresadoComponent } from './lista-ingresado/lista-ingresado.component';
import { JerarquiaComponent } from './jerarquia/jerarquia.component';
import { JerarquiasComponent } from './jerarquias/jerarquias.component';
import { StatusDocumentoComponent } from './status-documento/status-documento.component';
import { StatusDocumentosComponent } from './status-documentos/status-documentos.component';
import { RevisionAprobadosComponent } from './revision-aprobados/revision-aprobados.component';
import { RevisionBorradoresComponent } from './revision-borradores/revision-borradores.component';
import { RevisionIngresadosComponent } from './revision-ingresados/revision-ingresados.component';
import { RevisionPublicadosComponent } from './revision-publicados/revision-publicados.component';
import { HomeComponent } from './home/home.component';
import { IngresoDocumentoComponent } from './ingreso-documento/ingreso-documento.component';

@NgModule({
  declarations: [
    AppComponent,
    JerarquiaGeograficaComponent,
    JerarquiaContextoComponent,
    JerarquiaOrganizacionalComponent,
    DocumentosComponent,
    EmpresaComponent,
    FormularioComponent,
    ListaAprobadoComponent,
    ListaDocumentosComponent,
    ListaPublicadosComponent,
    ListaPublicadoComponent,
    ListaRevisadoComponent,
    ListaIngresadoComponent,
    JerarquiaComponent,
    JerarquiasComponent,
    StatusDocumentoComponent,
    StatusDocumentosComponent,
    RevisionAprobadosComponent,
    RevisionBorradoresComponent,
    RevisionIngresadosComponent,
    RevisionPublicadosComponent,
    HomeComponent,
    IngresoDocumentoComponent
  ],
  imports: [
    BrowserModule,
    ListViewModule,
    CheckBoxModule,
    HttpClientModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    NgbModule.forRoot(),
    FormsModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    TreeViewModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
