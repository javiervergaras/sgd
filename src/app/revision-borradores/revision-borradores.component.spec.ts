import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionBorradoresComponent } from './revision-borradores.component';

describe('RevisionBorradoresComponent', () => {
  let component: RevisionBorradoresComponent;
  let fixture: ComponentFixture<RevisionBorradoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisionBorradoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionBorradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
