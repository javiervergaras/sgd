import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusDocumentoComponent } from './status-documento.component';

describe('StatusDocumentoComponent', () => {
  let component: StatusDocumentoComponent;
  let fixture: ComponentFixture<StatusDocumentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusDocumentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusDocumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
