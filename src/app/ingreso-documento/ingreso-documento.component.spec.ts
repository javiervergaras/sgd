import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoDocumentoComponent } from './ingreso-documento.component';

describe('IngresoDocumentoComponent', () => {
  let component: IngresoDocumentoComponent;
  let fixture: ComponentFixture<IngresoDocumentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoDocumentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoDocumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
