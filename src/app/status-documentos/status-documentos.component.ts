import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-status-documentos',
  templateUrl: './status-documentos.component.html',
  styleUrls: ['./status-documentos.component.css']
})
export class StatusDocumentosComponent implements OnInit {
  studentsData: any;
  docs:any;
  empresas:any;
  docdetalle:any = {};

  async getAllStudents() {
    //Get saved list of students
    this.studentsData = await this.apiService.getDocumentos(1).toPromise();
  }

  async change(e:any){
    console.log(e.target.value);
    this.studentsData = [];
    this.studentsData = await this.apiService.getDocumentos(e.target.value).toPromise();
  }

  refresh(){
   this.getAllStudents();
  }

  async cargaempresas(){
    this.empresas = await this.apiService.getEmpresas().toPromise();
    console.log(this.empresas);
  }

  constructor(
    private toastr: ToastrService,
    public apiService: ApiService,
    private modalService: NgbModal,
    ) {
    this.studentsData = [];
    this.docs = [];
    this.cargaempresas();
   }

   async pasarRevision(id:string,idempresa:string){
    let _mensaje:any = await this.apiService.setRevision(id,"1",idempresa).toPromise();
    console.log(idempresa);
    console.log(_mensaje);
    if(_mensaje.estado == 1){
     this.toastr.success(_mensaje.mensaje,'Acción realizada');
    }else{
     this.toastr.error(_mensaje.mensaje,'Acción no realizada');
    }
    
 }

detalle(el,modal){
console.log(el);
this.docdetalle = el;
this.modalService.open(modal);
}

   verDoc(id){
    console.log(id);
    this.apiService.getDocument(id).subscribe(response => {
      this.docs = response;
      for (let entry of this.docs) {
        console.log(entry.documento); // 1, "string", false
        const blob = this.b64toBlob(entry.documento, 'application/octet-stream');
        console.log(blob);
        let url = window.URL.createObjectURL(blob);
        const file = new File([blob], entry.nombre, { type: 'application/octet-stream' });

    }
    
  })
   }

   b64toBlob (b64Data, contentType='', sliceSize=512){
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
  
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
  
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
  
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
      
    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }
  
  

  ngOnInit() {
    this.getAllStudents();
    console.log("Paso por metodo");
    this.cargaempresas();
  }
  elements: any = [
    {id: 1, first: 'Mark', last: 'Otto', handle: '@mdo'},
    {id: 2, first: 'Jacob', last: 'Thornton', handle: '@fat'},
    {id: 3, first: 'Larry', last: 'the Bird', handle: '@twitter'},
  ];

  headElements = ['ID', 'First', 'Last', 'Handle'];

}
