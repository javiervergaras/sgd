import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JerarquiaGeograficaComponent } from './jerarquia-geografica.component';

describe('JerarquiaGeograficaComponent', () => {
  let component: JerarquiaGeograficaComponent;
  let fixture: ComponentFixture<JerarquiaGeograficaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JerarquiaGeograficaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JerarquiaGeograficaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
