import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from './api.service';

declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  eleccion: string;
  geo: boolean;
  cont: boolean;
  org: boolean;
  destinos: string[];
  title = 'documental';
  arbol: any = {};
  arbol2: any = {};
  empresa: string;
  estado: string;
  estadoFlujo:string;
  codigodocumento: string;
  descripcion: string;
  tipodocumentoc: string;
  publicoprivado: number;
  confidencial: number;
  alcanceconfidencial: string;
  alcanceprivado: string;
  jeraGeo: string;
  jeraContext: string;
  jeraOrg: string;
  UsuarioGenera: string;
  fechaiimiteDef: string;
  fechalimite: string;
  varsel:number;
  doc:string;
  nombredoc:string;
  seleccionEdicion:string;
  statusSeleccionado:string;
  empresas:any;
  filestr:any;

  constructor(private toastr: ToastrService,public apiService: ApiService) {
    this.cargaEmpresas();
    this.destinos = ['Geografica', 'Contexto', 'Organizacional'];
    this.eleccion = "Geografica";
    this.geo = true;
    this.cont = false;
    this.org = false;
    this.empresa = "Empresa 3";
    this.varsel = 0;
    //$("input[name="+ name +"]").each( function () {});
  }

  async cargaEmpresas(){
    this.empresas = await this.apiService.getEmpresas().toPromise();
  }

  showSuccess() {
    this.toastr.success('El documento fue guardado exitosamente','Acción realizada');
  }

  cargaArbol(select: string,tipo:string) {
    var html = "";

    $(this.arbol).each(function () {
      html += `<ul id='arbol${select}'><li> <input type='checkbox' (click)="verificacheck('${tipo}',1,'${this.name}')" disabled class='checkbox' name="${tipo}" id='${this.id}' >  <label for='${this.id}'>${this.name}</label>`;
      html += generarHijos(this.children);
      html += "</li></ul>";
    });

    function generarHijos(obj) {
      var html = "<ul>";

      $(obj).each(function () {
        html += `<li> <input type='checkbox' onclick="verificacheck('${tipo}',${this.id},'${this.name}')" class='checkbox' name='${tipo}' id='${this.id}' >  <label for='${this.id}'>${this.name}</label>`;
        html += generarHijos(this.children);
        html += "</li>";
      });
      html += "</ul>";

      return html;
    }

    $("#arbol" + select).remove();
    $("#" + select).append(html);
    $('#arbol' + select).bonsai();
   
  }



  ngOnInit(): void {
    this.cargaEmpresas();
  }

  agregar(){
    $('.addNode').modal('show');
  }

  eliminar(){
    $('.removeNode').modal('show');
  }

  editar(){
    if($('#mantjerselEdit').html() != 0){
      var padre = $('#mantjersel').html();
      var nombre = $('#mantjerselEdit').html();
  
      $('#editNodo').val(nombre);
      $('.editNode').modal('show');
    }else{
      alert("Seleccione un nodo para continuar");

    }
    

  }



  verificacheck(name:string,value:string,sel:string){
    
    $("input[name="+ name +"]").each( function () {
      $(this).prop('checked',false);
    
    });

    $("input[name="+ name +"]").each( function () {


      if($(this).prop('value') == value ){
        $(this).prop('checked',true);
        $('#'+ name +'sel').html(value);
        $('#'+ name +'selEdit').html(sel);

      }
    });
  }



  mostrarJerarquias(){

    $('#principal').addClass('d-none');
    $('#MantJerarquias').removeClass('d-none');
    $('#MantJerCont').addClass('d-none');
    $('#tipoJer').html(1);
  }



  editarDoc(seleccion:any){
    this.seleccionEdicion = $('#seleccionEditar').html();
    this.statusSeleccionado =  ($('#estadoflujoEditar').val() == '' ? 'N/A' : $('#estadoflujoEditar').val());
    console.log(seleccion);
    console.log(seleccion.value);
    console.log(this.seleccionEdicion);
    console.log(this.statusSeleccionado);
    this.apiService.setStatus(this.seleccionEdicion,seleccion.value).subscribe((res)=>{
      this.toastr.success('El documento fue editado exitosamente','Acción realizada');
      $('#btnhideeditar').click();
      $('#refresh').click();
    });
  }


  changeListener($event) : void {
    this.readThis($event.target);
  }
  
  readThis(file:File): void {
    var myReader:FileReader = new FileReader();
  
    myReader.onloadend =  (e) => {
     this.filestr = myReader.result;
    }
    myReader.readAsDataURL(file);
  }

  async save_doc(files:any){
    let _file:File = files.files[0]; 
    //await this.readThis(_file);
    //console.log(this.filestr);
    let _mensaje = await this.apiService.setDocumentFS(_file);
    console.log(_mensaje);
  }

  guardaDoc() {
    console.log("Llego a la funcion");
    this.empresa = ($('#empresa').val() == '' ? 'N/A' : $('#empresa').val());
    this.estado =   ($('#estado').val() == '' ? 'N/A' : $('#estado').val());
    this.estadoFlujo =   ($('#estadoflujo').val() == '' ? 'N/A' : $('#estadoflujo').val());
    this.codigodocumento = ($('#codigodocumento').val() == '' ? 'N/A' : $('#codigodocumento').val());
    this.descripcion = ($('#descripcion').val() == '' ? 'N/A' : $('#descripcion').val());
    this.tipodocumentoc = ($('#tipodocumentoc').val() == '' ? 'N/A' : $('#tipodocumentoc').val());
    this.confidencial =  $('#confidencialsel').html();
    this.jeraGeo =  $('#geografiasel').html();
    this.jeraContext = $('#contextosel').html();
    this.jeraOrg =  $('#organizasel').html();
    this.UsuarioGenera = ($('#UsuarioGenera').val() == '' ? 'N/A' : $('#UsuarioGenera').val()) ;
    this.fechaiimiteDef = ($('#fechaiimiteDef').val() == '' ? '2019-01-01' : $('#fechaiimiteDef').val());
    this.nombredoc = $('#name').html();
    this.doc = $('#doc').html();
    
    $.ajax({
      data: {"empresa":this.empresa,"estado":this.estado, "codigodocumento":this.codigodocumento, "descripcion" :this.descripcion,"tipodocumento":this.tipodocumentoc, "publicoprivado":this.publicoprivado,"confidencial":this.confidencial,"alcanceconfidencial":this.alcanceconfidencial,"alcanceprivado":this.alcanceprivado,"jeraGeo":this.jeraGeo,"jeraContext":this.jeraContext,"jeraOrg":this.jeraOrg,"UsuarioGenera":this.UsuarioGenera,"fechaiimiteDef":this.fechaiimiteDef,"estadoFlujo" : this.estadoFlujo, "nombredoc":this.nombredoc,"doc":this.doc },
      //Cambiar a type: POST si necesario
      type: "POST",
      // Formato de datos que se espera en la respuesta
      dataType: "json",
      // URL a la que se enviará la solicitud Ajax
      url: "http://localhost:3000/setdocs",

      //type: "GET",
      //url: "",
      //data: ,
      //contentType: "application/json; charset=utf-8",
      //async: true,
      //dataType: "json",
      success: function (response) {
        $('#refresh').click();
        $('#empresa').val('');
        $('#estado').val('');
        $('#estadoflujo').val('');
        $('#codigodocumento').val('');
        $('#descripcion').val('');
        $('#tipodocumentoc').val('');
        $('#confidencialsel').html('');
        $('#geografiasel').html('');
        $('#contextosel').html('');
        $('#organizasel').html('');
        $('#UsuarioGenera').val('');
        $('#fechaiimiteDef').val('');
        $('#name').html('');
        $('#doc').html('');
      }

    });

    console.log({"empresa":this.empresa,"estado":this.estado, "codigodocumento":this.codigodocumento, "descripcion" :this.descripcion,"tipodocumento":this.tipodocumentoc, "confidencial":this.confidencial,"jeraGeo":this.jeraGeo,"jeraContext":this.jeraContext,"jeraOrg":this.jeraOrg,"UsuarioGenera":this.UsuarioGenera,"fechaiimiteDef":this.fechaiimiteDef, "nombredoc":this.nombredoc,"doc":this.doc });
    
    $('.bd-example-modal-lg').modal('hide');
    
    this.showSuccess();
    
  }

}