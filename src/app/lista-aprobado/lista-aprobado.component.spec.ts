import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAprobadoComponent } from './lista-aprobado.component';

describe('ListaAprobadoComponent', () => {
  let component: ListaAprobadoComponent;
  let fixture: ComponentFixture<ListaAprobadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaAprobadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAprobadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
