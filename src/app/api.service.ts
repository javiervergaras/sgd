import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse,HttpParams  } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  endpoint: string = 'http://localhost:3000';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  getDocumentos(empresa) {
    return this.http.post(`${this.endpoint}/docs`,{empresa:empresa} );
  }

  getBorradores(empresa) {
    return this.http.post(`${this.endpoint}/borradores`,{empresa:empresa});
  }

  getIngresados(empresa) {
    return this.http.post(`${this.endpoint}/ingresados`,{empresa:empresa} );
  }

  getRevisados() {
    return this.http.get(`${this.endpoint}/revisados` );
  }

  getAprobados(empresa) {
    return this.http.post(`${this.endpoint}/aprobados`,{empresa:empresa} );
  }

  getPublicados(empresa) {
    return this.http.post(`${this.endpoint}/publicados`,{empresa:empresa} );
  }

  getDocument(id:string) {
    return this.http.get(`${this.endpoint}/get_doc/${id}`);
  }

  setStatus(id,status){
   return this.http.post(`${this.endpoint}/editar_flujo`,{id:id,status:status});
  }

  setRevision(idDocumento:string, EstatusFlujo:string, idempresa:string){
    return this.http.post(`${this.endpoint}/set_revision`,{"idDocumento":idDocumento,"EstatusFlujo":EstatusFlujo,"idempresa":idempresa});
  }

  getRevisiones(empresa,tipo){
    return this.http.post(`${this.endpoint}/get_revisiones`,{empresa:empresa,tipo:tipo});
  }

  setNodo(padre, nombre, tipo, empresa){
    return this.http.post(`${this.endpoint}/set_Nodo`,{"padre": padre,"nombre": nombre,"empresa" : empresa,"tipo":tipo});
  }

  editNodo(id, nombre, tipo){
    return this.http.post(`${this.endpoint}/edit_Nodo`,{"id": id,"nombre": nombre,"tipo":tipo});
  }

  getEmpresas(){
    return this.http.post(`${this.endpoint}/get_empresas`,{}); 
  }
  
  setEmpresa(nombre,razon,direccion){
    return this.http.post(`${this.endpoint}/set_empresa`,{"nombre":nombre,"razon":razon,"direccion":direccion}) ;
  }

  existeempresa(razon){
    return this.http.post(`${this.endpoint}/get_verificaEmpresa`,{"razon":razon}) ;
  }

  delempresa(id){
    return this.http.post(`${this.endpoint}/del_empresa`,{"id":id}) ;
  }

  getJerarquia(empresa,tipo){
    return this.http.post(`${this.endpoint}/getLista`,{tipo:tipo,empresa:empresa}) ;
  }

  del_nodo(id,tipo){
    return this.http.post(`${this.endpoint}/del_nodo`,{id:id,tipo:tipo}) ;
  }

  upload_f(f,name){
    return this.http.post(`${this.endpoint}/upload_f`,{file:f,name:name}) ;
  }

  save_document(doc:any){
    return this.http.post(`${this.endpoint}/upload_doc`,{doc:doc}) ;
  }

  valida_doc(idempresa, codigo){
    return this.http.post(`${this.endpoint}/validadoc`,{idempresa:idempresa,codigo:codigo}) ;
  }

  download_file(filename){
    return this.http.post(`${this.endpoint}/testGetFile`,{fileName:filename}) ;
  }
  
  setDocumentFS(file:any) {
    const payload = new FormData();
    payload.append('name', "Pruebas");
    payload.append('image', file);
    console.log(payload);
    this.http
    .post(`${this.endpoint}/send`,
      payload, {
        headers: {
         
        }
      }
    ).subscribe((data: any) => {
      console.log(data);
    });
 // return  this.http.post(`${this.endpoint}/write`, {file:file}).toPromise();
}
  
}
