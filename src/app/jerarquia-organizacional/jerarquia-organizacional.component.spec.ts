import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JerarquiaOrganizacionalComponent } from './jerarquia-organizacional.component';

describe('JerarquiaOrganizacionalComponent', () => {
  let component: JerarquiaOrganizacionalComponent;
  let fixture: ComponentFixture<JerarquiaOrganizacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JerarquiaOrganizacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JerarquiaOrganizacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
