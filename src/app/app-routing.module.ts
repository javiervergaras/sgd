import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {ListaDocumentosComponent} from './lista-documentos/lista-documentos.component';
import {JerarquiasComponent} from './jerarquias/jerarquias.component';
import { ListaIngresadoComponent } from './lista-ingresado/lista-ingresado.component';
import { ListaRevisadoComponent } from './lista-revisado/lista-revisado.component';
import { ListaAprobadoComponent } from './lista-aprobado/lista-aprobado.component';
import { ListaPublicadoComponent } from './lista-publicado/lista-publicado.component';
import { DocumentosComponent } from './documentos/documentos.component';
import {StatusDocumentosComponent} from './status-documentos/status-documentos.component';

import {RevisionBorradoresComponent} from './revision-borradores/revision-borradores.component';
import {RevisionIngresadosComponent} from './revision-ingresados/revision-ingresados.component';
import {RevisionAprobadosComponent} from './revision-aprobados/revision-aprobados.component';
import {RevisionPublicadosComponent} from './revision-publicados/revision-publicados.component';
import {EmpresaComponent} from './empresa/empresa.component';
import {HomeComponent} from './home/home.component';
import {IngresoDocumentoComponent} from './ingreso-documento/ingreso-documento.component';
import {JerarquiaGeograficaComponent} from './jerarquia-geografica/jerarquia-geografica.component';
import {JerarquiaContextoComponent} from './jerarquia-contexto/jerarquia-contexto.component'
import {JerarquiaOrganizacionalComponent} from './jerarquia-organizacional/jerarquia-organizacional.component'

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path:'Documentos',component:IngresoDocumentoComponent},
  {path:'Borrador',component:ListaDocumentosComponent},
  {path:'Aprobado',component:ListaAprobadoComponent},
  {path:'Ingresado',component:ListaIngresadoComponent},
  {path:'Revisado',component:ListaRevisadoComponent},
  {path:'Publicado',component:ListaPublicadoComponent},
  {path:'MantenedorJerarquias',component:JerarquiasComponent},
  {path:'docs', component:DocumentosComponent},
  {path:'rborradores',component:RevisionBorradoresComponent},
  {path:'ringresados',component:RevisionIngresadosComponent},
  {path:'raprobados',component:RevisionAprobadosComponent},
  {path:'rpublicados',component:RevisionPublicadosComponent},
  {path:'mempresa',component:EmpresaComponent},
  {path:'home',component:HomeComponent},
  {path:'geografica',component:JerarquiaGeograficaComponent},
  {path:'contexto',component:JerarquiaContextoComponent},
  {path:'organizacional',component:JerarquiaOrganizacionalComponent},
  {path:'status',component:StatusDocumentosComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
