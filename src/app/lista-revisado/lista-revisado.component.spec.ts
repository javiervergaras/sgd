import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRevisadoComponent } from './lista-revisado.component';

describe('ListaRevisadoComponent', () => {
  let component: ListaRevisadoComponent;
  let fixture: ComponentFixture<ListaRevisadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRevisadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRevisadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
