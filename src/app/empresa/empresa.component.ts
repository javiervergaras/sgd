import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {
  empresas:any;
  closeResult: string;
  mensaje:string;
  modal:NgbModalRef;
  constructor(
    private toastr: ToastrService,
    public apiService: ApiService,
    private modalService: NgbModal
    ) {
      this.mensaje = "Error";
     }


  ngOnInit() {
    this.cargarEmpresas();
    

  }

  async cargarEmpresas(){
    this.empresas = await this.apiService.getEmpresas().toPromise();
    console.log(this.empresas);
  }

  open(content) {
    this.modal = this.modalService.open(content);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  async guardarEmpresa(nombre,razon,direccion,pop){
    console.log(nombre);
    console.log(razon);
    console.log(direccion);
    this.mensaje = "Favor ingresar ";
    if(nombre.length == 0){
     this.mensaje += "nombre, ";
    }
    if(razon.length == 0){
      this.mensaje += "razon social, ";
    }
    if(direccion.length == 0){
      this.mensaje += "dirección ";
    }
    this.mensaje += "Para continuar";
    if(nombre.length == 0 || razon.length == 0 || direccion.length == 0){
      this.open(pop);
    }else{
      let existe:any = await this.apiService.existeempresa(razon.trim()).toPromise();
      console.log(existe[0].count);
      console.log(existe);
      if(existe[0].count == 0){
        let guardo:any = await this.apiService.setEmpresa(nombre,razon.trim(),direccion).toPromise();
        this.cargarEmpresas();
        this.modal.dismiss();
        this.toastr.success('La empresa se creo satisfactoriamente','Acción realizada');
      }else{
        this.modal.dismiss();
        this.toastr.error('La empresa ya existe o tiene algun problema, favor contactar al administrador','Acción no realizada');
      }
      
    }
    
  }

  async del_empresa(id){
    await this.apiService.delempresa(id).toPromise();
    this.toastr.success('Empresa eliminada','Acción realizada');
    await this.cargarEmpresas();
  }

}
