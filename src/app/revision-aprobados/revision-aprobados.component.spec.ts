import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionAprobadosComponent } from './revision-aprobados.component';

describe('RevisionAprobadosComponent', () => {
  let component: RevisionAprobadosComponent;
  let fixture: ComponentFixture<RevisionAprobadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisionAprobadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionAprobadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
